using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Collections;

namespace Xtudio16.FSM.Editor
{
    public class FSMEditor: EditorWindow{
	
        #region Window Invocation
        static FSMEditor window;
        public static void Init(FSM currentFSMEdited)
        {
            if(window)
                window.Close();

            window = EditorWindow.GetWindow<FSMEditor>();
            window.MyFSM = currentFSMEdited;
			window.title = currentFSMEdited.Id;
            window.Start();
	   
        }
        #endregion
	

        public FSM MyFSM;
        public FSMEditorState [] EditorStates;
        public Dictionary<string,FSMEditorState> EditorStatesIndex;
        FSMStatesControl FSMStatesControl;
        FSMCreateStateControl FSMCreateStateControl;
		FSMGoToParentFSMControl FSMGoToParentFSMControl;
	
        public void Start()
        {
		
            this.title = "Xtudio 16 FSM Editor";
            var states = MyFSM.GetStatesFromChildren();
            CreateEditorStates (states);
            FSMStatesControl = new FSMStatesControl(this);
            FSMCreateStateControl = new FSMCreateStateControl(this);
			FSMGoToParentFSMControl = new FSMGoToParentFSMControl (this);
        }

		void CreateEditorStates (List<FSMState> states)
		{
			EditorStates = new FSMEditorState[states.Count];
			EditorStatesIndex = new Dictionary<string, FSMEditorState> ();
			int id = 0;
			foreach (var state in states) {
				var editorState = new FSMEditorState (this,state, id);
				EditorStates [id] = editorState;
				EditorStatesIndex.Add (state.StateName, editorState);
				id++;
			}
		}

        bool ValidateIntegrity()
        {
            if (EditorStates == null || FSMCreateStateControl==null|| FSMStatesControl==null)
            {   
                if(this.MyFSM!=null)
                    FSMEditor.Init(this.MyFSM);
                else
                    this.Close();
              
                return false;
            }
            return true;
        }
 
        void OnGUI()
        {
	      
            if (!ValidateIntegrity()) return;
            FSMEditorEvent.Instance.CatchEvent();
            FSMCreateStateControl.Render();
            FSMStatesControl.Render();
			FSMGoToParentFSMControl.Render ();
      
        }

		public FSM GetParentFSM()
		{
			var state =MyFSM.transform.GetComponentInParent<FSMState> ();
			if (state != null) {
				var parentFSM = state.GetComponentInParent<FSM>();
				return parentFSM;
			}
			return null;
		}
    }
}