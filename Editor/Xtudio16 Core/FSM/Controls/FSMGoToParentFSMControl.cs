﻿using UnityEngine;
using System.Collections;

namespace Xtudio16.FSM.Editor
{
	public class FSMGoToParentFSMControl
    {

        public FSMEditor MyFSMEditor;

		public FSMGoToParentFSMControl(FSMEditor MyFSMEditor)
        {
            this.MyFSMEditor = MyFSMEditor;
        }

        public void Render()
        {
			var parentFSM = MyFSMEditor.GetParentFSM ();

			if (parentFSM != null) {
								
				if (GUI.Button (new Rect (120, 20, 100, 50), "Go Up"))
						 FSMEditor.Init (parentFSM);
						
			}

        }
    }
}