﻿using UnityEngine;
using System.Collections;

namespace Xtudio16.FSM.Editor
{
    public class FSMStateMenuControl  {

        public FSMStatesControl MyFSMStatesControl;

        public FSMEditorState DraggedState;
        public int DraggedTransitionIndex = -1;

		public FSMStateMenuControl(FSMStatesControl MyFSMStatesControl)
        {
            this.MyFSMStatesControl = MyFSMStatesControl;
        }

        
		public void Render()
		{

			if (FSMEditorEvent.Instance.EditorEventType == FSMEditorEventType.MouseDown&&
			    FSMEditorEvent.Instance.MouseButton==1)
			{

				var clickedState =MyFSMStatesControl.FindStateUnderMouse();
				if(clickedState!=null)
				{
					//FSMStateMenuWindow window = new FSMStateMenuWindow();

					FSMStateMenuWindow.InitPopup(clickedState);
				}
			}
		     		
		}
      


    }
}