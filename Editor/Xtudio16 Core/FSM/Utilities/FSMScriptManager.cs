﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.IO;

namespace Xtudio16.FSM.Editor
{
    public class FSMScriptManager  {


        public static void CreateFSMScript(string className,string nameSpace,string path="Assets/",string baseClass="FSMState")
        {

            string classCode =
                @"
using UnityEngine;
using System.Collections;
using Xtudio16.FSM;

namespace "+nameSpace+@"
{
    public class " + className+" : "+baseClass+ @"{
 
        public override void Enter(FSMState lastState)
        {
            base.Enter(lastState);
        }

        public override void Process ()
	    {
		    
	    }

        public override void Leave(FSMState nextState)
        {
            base.Leave(nextState);
        }
    }
}";

            if(baseClass!="FSMState" && !FSMScriptManager.ScriptExists(baseClass))
                FSMScriptManager.CreateFSMScript(baseClass,nameSpace,path);

            string copyPath = path+className+".cs";
            Debug.Log("Creating Classfile: " + copyPath);
 
            if( File.Exists(copyPath) == false ){ // do not overwrite
                using (StreamWriter outfile = 
                    new StreamWriter(copyPath))
                {
                    outfile.WriteLine(classCode);
                  
                }//File written
            }   
   
        }

		public static void CreateFSMExtensionScript(string className,string nameSpace,string path="Assets/",string baseClass="FSMStateExtension")
		{
			
			string classCode =
				@"
using UnityEngine;
using System.Collections;
using Xtudio16.FSM;

namespace "+nameSpace+@"
{
    public class " + className+" : "+baseClass+ @"{
 
      
        protected override void Process ()
	    {
		    
	    }

       
    }
}";
			
			if(baseClass!="FSMStateExtension" && !FSMScriptManager.ScriptExists(baseClass))
				FSMScriptManager.CreateFSMExtensionScript(baseClass,nameSpace,path);
			
			string copyPath = path+className+".cs";
			Debug.Log("Creating Classfile: " + copyPath);
			
			if( File.Exists(copyPath) == false ){ // do not overwrite
				using (StreamWriter outfile = 
				       new StreamWriter(copyPath))
				{
					outfile.WriteLine(classCode);
					
				}//File written
			}   
			
		}

        public static bool ScriptExists(string className)
        {
            var scripts = GetAllFSMStateScripts();
            return scripts.Contains(className);
        }

		public static bool ScriptExtensionExists(string className)
        {
			var scripts = GetExtensionsFSMStateScripts();
            return scripts.Contains(className);
        }

        static bool IsValidFSMScript(MonoScript monoScriptAsset )
        {
       
            var monoScriptType = monoScriptAsset.GetClass();
            if (monoScriptType == null) return false;

            var parentType = monoScriptType.BaseType;
            while (parentType != null)
            {
                if (parentType.Name == "FSMState")
                    return true;
                parentType = parentType.BaseType;
            }

            return false;

        }

		static bool IsValidScript(MonoScript monoScriptAsset,string parentClass )
		{
			
			var monoScriptType = monoScriptAsset.GetClass();
			if (monoScriptType == null) return false;
			
			var parentType = monoScriptType.BaseType;
			while (parentType != null)
			{
				if (parentType.Name == parentClass)
					return true;
				parentType = parentType.BaseType;
			}
			
			return false;
			
		}


		public static void OpenScript(string className)
		{
			//AssetDatabase.OpenAsset (script);
			//TODO: considerar esto?
			//UnityEditorInternal.MonoScripts.CreateMonoScript

			UnityEditorInternal.InternalEditorUtility.OpenFileAtLineExternal (GetScriptPath(className),1);
		}

	    public static string GetScriptPath(string className)
		{

			var scriptPaths = Directory.GetFiles("Assets", "*.cs", SearchOption.AllDirectories);
			foreach (var scriptPath in scriptPaths)
			{
				var scriptAsset = AssetDatabase.LoadAssetAtPath(scriptPath, typeof (UnityEngine.Object));
				if (!(scriptAsset is MonoScript)) continue;
				var monoScriptAsset = (MonoScript) scriptAsset;
				if(monoScriptAsset.GetClass().Name==className)
					return scriptPath;
			}
			return "";	
		}

        public static List<String> GetAllFSMStateScripts()
        {
            var scriptList = new List<string>();

            var scriptPaths = Directory.GetFiles("Assets", "*.cs", SearchOption.AllDirectories);
            foreach (var scriptPath in scriptPaths)
            {
                var scriptAsset = AssetDatabase.LoadAssetAtPath(scriptPath, typeof (UnityEngine.Object));
                if (!(scriptAsset is MonoScript)) continue;
                var monoScriptAsset = (MonoScript) scriptAsset;
                if (IsValidFSMScript(monoScriptAsset))
                    scriptList.Add(monoScriptAsset.GetClass().Name);
            }
            return scriptList;
        }

		public static List<String> GetExtensionsFSMStateScripts()
		{
			var scriptList = new List<string>();
			
			var scriptPaths = Directory.GetFiles("Assets", "*.cs", SearchOption.AllDirectories);
			foreach (var scriptPath in scriptPaths)
			{
				var scriptAsset = AssetDatabase.LoadAssetAtPath(scriptPath, typeof (UnityEngine.Object));
				if (!(scriptAsset is MonoScript)) continue;
				var monoScriptAsset = (MonoScript) scriptAsset;
				if (IsValidScript(monoScriptAsset,"FSMStateExtension"))
					scriptList.Add(monoScriptAsset.GetClass().Name);
			}
			return scriptList;
		}

    }
}