using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Xtudio16.FSM.Editor
{
    [CustomEditor(typeof(FSM))]
    class FSMGraphEditor : UnityEditor.Editor {
 
	
        public override void OnInspectorGUI() {
	 
            var fsm = (FSM)target;


			GUILayout.Label("FSM Id:");
			fsm.Id = GUILayout.TextField (fsm.Id);
            fsm.AutoStart = GUILayout.Toggle(fsm.AutoStart, "Auto Start");
            if(fsm.CurrentState&&fsm.IsEnabled)	
                GUILayout.Label("Current State:"+fsm.CurrentState.StateName);
            else
                GUILayout.Label("FSM is inactive");
            if(GUILayout.Button("Launch FSM Editor"))
                FSMEditor.Init(fsm);
        }
    }
}