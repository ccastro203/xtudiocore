﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection.Emit;
using System.Threading;

using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Linq;

namespace Xtudio16.FSM.Editor
{

	//TODO: clean this class
	public class FSMAddScriptWindow : EditorWindow {

        #region Window Invocation
		static FSMAddScriptWindow window;
       

		public static void InitPopup(FSMEditorState selectedState)
		{
		
			Rect area = new Rect (FSMEditorEvent.Instance.MousePosition.x,FSMEditorEvent.Instance.MousePosition.y+100,400,300 );
			if (window)
				window.Close();
			
			window = EditorWindow.GetWindow<FSMAddScriptWindow>();

			window.position = area;
			window.title = selectedState.State.StateName;
			window.SelectedState = selectedState;
			window.Start ();
			window.Show ();
		}
        #endregion

    
		public FSMEditorState SelectedState;
       
		private string newScriptName;
		private int selectedScriptCreationOptionIndex = 0;
		private int selectedExistingScriptIndex = 0;
		private string [] scriptList; 
		private bool isScriptCompiling = false;
		private string baseScript="FSMStateExtension";

        void Start()
        {
          
			LoadScriptList();
        }

		void LoadScriptList()
		{
			scriptList = FSMScriptManager.GetExtensionsFSMStateScripts().ToArray();
			Array.Sort<string>(scriptList);
		}

       

        void Update()
        {
            if (isScriptCompiling) {
				if (FSMScriptManager.ScriptExtensionExists(newScriptName))
				{
					isScriptCompiling = false;
					UnityEngineInternal.APIUpdaterRuntimeServices.AddComponent(SelectedState.State.gameObject, "Assets/Xtudio/Editor/Xtudio16 Core/FSM/Windows/FSMAddScriptWindow.cs (69,6)", newScriptName);
					var extension = (FSMStateExtension) SelectedState.State.gameObject.GetComponent(newScriptName);
					extension.StateToExtend = SelectedState.State;
					FSMScriptManager.OpenScript(newScriptName);
					this.Close();
				}			
			}
          
        }

		void ProcessAddScript ()
		{
			if (selectedExistingScriptIndex == 0) 
			{
				if (!FSMScriptManager.ScriptExtensionExists(newScriptName))
				{
					FSMScriptManager.CreateFSMExtensionScript(newScriptName,SelectedState. MyFSMEditor.MyFSM.DefaultNameSpace,SelectedState. MyFSMEditor.MyFSM.DefaultScriptPath, baseScript);
					AssetDatabase.Refresh();
					isScriptCompiling = true;
					
				}
				else
					Debug.LogError("Script " + newScriptName + " already exists!!!");	
						
			} 
			else 
			{
				UnityEngineInternal.APIUpdaterRuntimeServices.AddComponent(SelectedState.State.gameObject, "Assets/Xtudio/Editor/Xtudio16 Core/FSM/Windows/FSMAddScriptWindow.cs (96,5)", scriptList[selectedExistingScriptIndex]);
				var extension = (FSMStateExtension) SelectedState.State.gameObject.GetComponent(scriptList[selectedExistingScriptIndex]);
				extension.StateToExtend = SelectedState.State;
				FSMScriptManager.OpenScript(scriptList[selectedExistingScriptIndex]);
				this.Close();
			}
		}

        void OnGUI()
        {

			GUILayout.Label("Create a new state", EditorStyles.boldLabel);
			SelectedState.MyFSMEditor.MyFSM.DefaultScriptPath = EditorGUILayout.TextField("Script Path",SelectedState. MyFSMEditor.MyFSM.DefaultScriptPath);
			baseScript = EditorGUILayout.TextField("Base Class", baseScript);
			SelectedState.MyFSMEditor.MyFSM.DefaultNameSpace = EditorGUILayout.TextField("Namespace", SelectedState.MyFSMEditor.MyFSM.DefaultNameSpace);

			selectedScriptCreationOptionIndex = EditorGUILayout.Popup("Script Selection", selectedScriptCreationOptionIndex,
			                                                          new string[] {"New Script", "Use Existing Script"});
			if(selectedScriptCreationOptionIndex==0)
				newScriptName = EditorGUILayout.TextField("Script Name", newScriptName);
			else
				selectedExistingScriptIndex = EditorGUILayout.Popup("Script", selectedExistingScriptIndex, scriptList);
			
			
			if (!isScriptCompiling)
			{
				if (GUILayout.Button("Add"))
					ProcessAddScript();
			}
			else
				GUILayout.Label("Please wait while the script compiles :)");

        }

  

    }
}