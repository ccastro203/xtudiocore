﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection.Emit;
using System.Threading;

using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Linq;

namespace Xtudio16.FSM.Editor
{

	//TODO: clean this class
	public class FSMStateMenuWindow : EditorWindow {

        #region Window Invocation
		static FSMStateMenuWindow window;
        public static void Init()
        {
            if (window)
                window.Close();

			window = EditorWindow.GetWindow<FSMStateMenuWindow>();

			window.Show ();
        }

		public static void InitPopup(FSMEditorState selectedState)
		{
		
			Rect area = new Rect (FSMEditorEvent.Instance.MousePosition.x,FSMEditorEvent.Instance.MousePosition.y+100,200,300 );
			if (window)
				window.Close();
			
			window = EditorWindow.GetWindow<FSMStateMenuWindow>();

			window.position = area;
			window.title = selectedState.State.StateName;
			window.SelectedState = selectedState;
			window.Show ();
		}
        #endregion

    
		public FSMEditorState SelectedState;
       

        void Start()
        {
          
           
        }

       

       

        void Update()
        {
            
          
        }


        void OnGUI()
        {
			bool hasInternalFSM = false;
			var scripts =   SelectedState.State.GetComponents<MonoBehaviour> ();
			GUILayout.Label ("View Scripts");
            foreach (var script in scripts) 
			{
                
				if(script is FSMStateWithInternalFSM)
				{
					hasInternalFSM = true;
					continue;
				}
				if(GUILayout.Button(script.GetType().Name))
				{
					FSMScriptManager.OpenScript(script.GetType().Name);
					this.Close();
				}

			}
			if (hasInternalFSM) {
				GUILayout.Label ("Internal FSM");
				if(GUILayout.Button("View"))
				{
					FSM internalFSM = SelectedState.GetInternalFSM();
					if(internalFSM!=null)
						FSMEditor.Init(internalFSM);
					else
						Debug.LogError("No Internal FSM found");
					this.Close();
				}
			}

			if(GUILayout.Button("Add Script"))
			{

				FSMAddScriptWindow.InitPopup(SelectedState);
				this.Close();
			}

			if(GUILayout.Button("Select"))
			{
				
				AssetDatabase.OpenAsset(SelectedState.State);
				this.Close();
			}


        }

  

    }
}