using UnityEngine;
using System.Collections;

namespace Xtudio16.Actions
{
    public class ActionFSMSendMessage : Action {
	
        public FSM.FSM MyFSM;
        public string MethodName;
        public string [] Arguments; 
        public override void ActionPerformed ()
        {
            MyFSM.SendMessageToCurrentState(MethodName,Arguments);
        }
	
    }
}