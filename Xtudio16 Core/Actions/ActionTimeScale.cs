using UnityEngine;
using System.Collections;

namespace Xtudio16.Actions
{
    public class ActionTimeScale : Action {

        public float TimeScale;
	
        public override void ActionPerformed ()
        {
            Time.timeScale = TimeScale;
        }
    }
}