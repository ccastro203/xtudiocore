using UnityEngine;
using System.Collections;

namespace Xtudio16.Actions
{
    public class ActionFSMRaiseEvent : Action {
	
        public FSM.FSM MyFSM;
	
        public string EventName;
	
	
        public override void ActionPerformed ()
        {
            MyFSM.RaiseEvent(EventName);
        }
	
	
	
	
    }
}