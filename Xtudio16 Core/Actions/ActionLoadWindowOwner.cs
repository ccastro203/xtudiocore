using UnityEngine;
using System.Collections;
using Xtudio16.UI;

namespace Xtudio16.Actions
{
    public class ActionLoadWindowOwner : ActionLoadWindow {

        public GameObject LoadButton;
	
        public override void ActionPerformed ()
        {
            var parent =  WindowLoader.Show(WindowPrefabName,WindowParent);	
		
            var script =  parent.GetComponentInChildren<ActionShowLoadOwner>();
            if(script!=null)
                script.Target = LoadButton;
            else
                Debug.LogWarning("No ActionShowLoadOwner script was found");
		
        }
	
	
	
    }
}