﻿using UnityEngine;
using System.Collections;

namespace Xtudio16.Actions
{
    public class ActionLoadScene : Action
    {

        public string SceneName;

        public override void ActionPerformed()
        {
            Application.LoadLevel (SceneName);
        }
    }
}