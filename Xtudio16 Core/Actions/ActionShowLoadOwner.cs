using UnityEngine;
using System.Collections;

namespace Xtudio16.Actions
{
    public class ActionShowLoadOwner : Action {
	
        public GameObject Target;	
	
        public override void ActionPerformed ()
        {
            Debug.Log("mmm");
            if(Target!=null)
                Target.SetActive(true);
            else
                Debug.LogWarning("No owner was specified");
	
        }
	
    }
}