using UnityEngine;
using System.Collections;
using Xtudio16.UI;

namespace Xtudio16.Actions
{
    public class ActionLoadWindow : Action {
	
        public string WindowPrefabName;
	
        public Transform WindowParent;
	
	
        public override void ActionPerformed ()
        {
            WindowLoader.Show(WindowPrefabName,WindowParent);
        }
	
    }
}