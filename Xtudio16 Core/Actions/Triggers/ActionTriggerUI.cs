using UnityEngine;
using System.Collections;

namespace Xtudio16.Actions.Triggers
{
    public class ActionTriggerUI : ActionTrigger {

        public enum UIActionEvent{OnClick,OnHoverIn,OnHoverOut,OnPressDown,OnPressUp};
        public UIActionEvent EventTrigger;
		
        void OnClick()
        {
            if(EventTrigger==UIActionEvent.OnClick)
                ExecuteAllActions();
        }
	
    
    }
}