using UnityEngine;
using System.Collections;

namespace Xtudio16.Actions.Triggers
{
    public class ActionTrigger : MonoBehaviour {
	
	
	
	
        public Action [] actions;
	
	
	
        void Awake () {
            actions = transform.GetComponentsInChildren<Action>();
        }
	
        public void ExecuteAllActions()
        {
            foreach(var action in actions)
            {
                action.ActionTriggered();
            }
        }
	
	
    }
}