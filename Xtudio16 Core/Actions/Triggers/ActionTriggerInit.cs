using UnityEngine;
using System.Collections;

namespace Xtudio16.Actions.Triggers
{
    public class ActionTriggerInit : ActionTrigger {

        // Use this for initialization
        void Start () {
            ExecuteAllActions();
        }
	
    }
}