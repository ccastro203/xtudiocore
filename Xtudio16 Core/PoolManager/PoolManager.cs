﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Object = UnityEngine.Object;

namespace Xtudio16
{

    namespace Xtudio16.Pools
    {


        [Serializable]
        public class PoolManagerItem
        {
            public GameObject Prefab;
            public int Quantity;
        }

        public class PoolManager : MonoBehaviour
        {

            public PoolManagerItem[] Items;
            public bool DestroyOnSceneChange = false;

            public static PoolManager ActiveInstance;

            private readonly SortedDictionary<string, Queue<GameObject>> _cachedGameObjects =
                new SortedDictionary<string, Queue<GameObject>>();


            // Use this for initialization
            private void Awake()
            {
                ActiveInstance = this;
                InstantiateAllGroups();
                if (!DestroyOnSceneChange)
                    DontDestroyOnLoad(gameObject);
            }


            private void InstantiateAllGroups()
            {
                foreach (var item in Items)
                {
                    var instancedObjects = new Queue<GameObject>();
                    for (int i = 0; i < item.Quantity; i++)
                    {
                        var instancedObject = (GameObject) Instantiate(item.Prefab);
                        instancedObject.SetActive(false);
                        instancedObject.transform.parent = transform;
                        instancedObjects.Enqueue(instancedObject);
                    }
                    Debug.Log(item.Prefab.tag);
                    _cachedGameObjects[item.Prefab.tag] = instancedObjects;
                }
            }

            public GameObject GetAnObject(string tagName)
            {

                var instancedGroup = _cachedGameObjects[tagName];
                if (instancedGroup.Count == 0)
                    throw new Exception("Run out of instances objects for " + tagName + ",please add more!");
                var objectToReturn = instancedGroup.Dequeue();
                objectToReturn.transform.parent = null;
                objectToReturn.transform.position = new Vector3(0, 0, 0);
                objectToReturn.SetActive(true);
                return objectToReturn;
            }


            public void FreeAnObject(GameObject objectToDelete)
            {
                objectToDelete.transform.parent = transform;
                objectToDelete.transform.position = new Vector3(0, 0, 0);
                objectToDelete.SetActive(false);
                var instancedGroup = _cachedGameObjects[objectToDelete.tag];
                instancedGroup.Enqueue(objectToDelete);

            }

        }

    }
}