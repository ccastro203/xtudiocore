namespace Xtudio16.FSM
{
    public class FSMStateWithInternalFSM : FSMState
    {
        public FSM InternalFSM;

        public override void Enter(FSMState lastState)
        {
            InternalFSM.StartFSM();
        }

        public override void Process()
        {

        }

        public override void Leave(FSMState nextState)
        {
            InternalFSM.StopFSM();
        }

    }
}