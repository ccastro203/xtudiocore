using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Xtudio16.FSM
{
    public class FSM : MonoBehaviour {
	
    
	
        Dictionary<string,FSMState>  States;
        Dictionary<string,FSMState>  EventsTransitions;
        FSMState InitialState;
        bool _isEnabled = false;
		static SortedDictionary<string,FSM> _activeFsmList = new SortedDictionary<string, FSM> (); 
	
        public FSMState CurrentState;
        
        public bool AutoStart = true;
        public string DefaultScriptPath = "Assets";
        public string DefaultNameSpace = "Xtudio16";
        public string BaseStateScriptClass = "FSMState";
		public string Id= "";
	
        public string CurrentProcessingEvent ="";



        void Awake ()
        {
            if(AutoStart&&!IsEnabled)
                StartFSM();
        }

        private void Setup()
        {
            EventsTransitions = new Dictionary<string, FSMState>();
            States = new Dictionary<string, FSMState>();
            BuildStates();
            BuildTransitions();
        }

        private void BuildStates()
        {
            var statesList = GetStatesFromChildren();

            foreach (var state in statesList)
            {
                States.Add(state.StateName, state);
                if (state.StartState)
                {
                    InitialState = state;
                }
            }
        }

        private void BuildTransitions()
        {
            foreach(var state in States.Values)
                foreach(var transition in state.Transitions)
                    if(States.ContainsKey(transition.ToState))
                        EventsTransitions.Add(state.StateName+":"+transition.EventName ,States[transition.ToState]);
        }
	
	    

        void Update () {
            if(_isEnabled)
               CurrentState.Process();            
        }
	
        void ChangeState(FSMState nextState)
        {
		
            CurrentState.Leave(nextState);
            CurrentState.SendMessage("OnLeave", SendMessageOptions.DontRequireReceiver);
            nextState.Enter(CurrentState);
            nextState.SendMessage("OnEnter", SendMessageOptions.DontRequireReceiver);
            CurrentState = nextState;
        }

        #region EventHandling

        public void RaiseEvent(FSMEvent evnt)
        {
            if (!_isEnabled)
            {
                Debug.LogWarning("Event " + evnt.Name + " should not be trigger,because the FSM is inactive");
                return;
            }

            if(evnt!=null)
            {
                if(EventsTransitions.ContainsKey(CurrentState.StateName+":"+evnt.Name))
                {
                    FSMState nextState = EventsTransitions[CurrentState.StateName+":"+evnt.Name];
                    CurrentProcessingEvent = evnt.Name;
                    ChangeState(nextState);
                }
                else 
                    Debug.LogWarning("Event "+evnt.Name+" not mapped at "+CurrentState.StateName);
            }
		
        }
	
        public void RaiseEvent(string eventName)
        {
            RaiseEvent(new FSMEvent(eventName));
        }
	
	   
        public void SendMessageToCurrentState(string methodName )
        {
            if(CurrentState!=null&&_isEnabled)
                CurrentState.SendMessage(methodName,SendMessageOptions.DontRequireReceiver);
        }
	
        public void SendMessageToCurrentState(string methodName,object value)
        {
            if (CurrentState != null && _isEnabled)
                CurrentState.SendMessage(methodName,value,SendMessageOptions.DontRequireReceiver);
		
        }

        #endregion


        public void StartFSM()
        {
			_activeFsmList.Add (Id, this);
            _isEnabled = true;
            Setup();
            CurrentState = InitialState;
            CurrentState.Enter(null);
            CurrentState.SendMessage("OnEnter",SendMessageOptions.DontRequireReceiver);
        }

        public void StopFSM()
        {
			_activeFsmList.Remove (Id);
            _isEnabled = false;
            CurrentState.Leave(null);
            CurrentState.SendMessage("OnLeave", SendMessageOptions.DontRequireReceiver);
        }

        public bool IsEnabled { get { return _isEnabled; } }

        public List<FSMState> GetStatesFromChildren()
        {
            var states =new List<FSMState>();
            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                var childState = child.GetComponent<FSMState>();
                if(childState!=null)
                    states.Add(childState);
            }
            return states;
        }

		public static FSM GetFSMById(string id)
		{

			return _activeFsmList[id];
		}
	
	
    }
}