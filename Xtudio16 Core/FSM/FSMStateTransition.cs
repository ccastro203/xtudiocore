using UnityEngine;
using System.Collections;
using System;

namespace Xtudio16.FSM
{
    [Serializable()]
    public class FSMStateTransition  {
	
        public string EventName;
        public string  ToState;
	
	
        public FSMStateTransition()
        {
            EventName ="";
            ToState="";
		
        }
	
    }
}