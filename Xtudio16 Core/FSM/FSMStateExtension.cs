using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace Xtudio16.FSM
{
    public abstract class FSMStateExtension : MonoBehaviour
    {

        public FSMState StateToExtend;
        private bool _enabled=false;

        protected abstract void Process();

        void Update()
        {
            if (_enabled)
                Process();

        }


        void OnEnter()
        {
            _enabled = true;
        }

        void OnLeave()
        {
            _enabled = false;
        }

        public void RaiseEvent(string eventName)
        {
            StateToExtend.RaiseEvent(eventName);
        }

        

    }
}