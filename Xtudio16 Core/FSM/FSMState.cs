using UnityEngine;

using System.Collections;
using System.Collections.Generic;

namespace Xtudio16.FSM
{
    public abstract class FSMState : MonoBehaviour {
	
	
        public bool StartState;
        protected FSM MyFSM;
	
        //Esta variable es usada en el editor para recordar donde fue posicionada la ultima ves
        [HideInInspector()]
        public Rect Frame;
        //Esta variable es usada en el editor para recordar  sus transiciones
        [HideInInspector]
        public List<FSMStateTransition> Transitions;
	
        public virtual void Process()
        {
            
        }
	
        public void Awake()
        {
            MyFSM = transform.parent.gameObject.GetComponent<FSM>();
        }
	
        public virtual void Enter(FSMState lastState)
        {
        }
        public virtual void Leave(FSMState nextState)
        {
        }

        public void RaiseEvent(FSMEvent evnt)
        {
            MyFSM.RaiseEvent(evnt);
        }

        public void RaiseEvent(string eventName)
        {
            MyFSM.RaiseEvent(new FSMEvent(eventName));
        }
	
        public string StateName
        {
            get
            {
                return name;
            }
        }

        //public bool IsActive
        //{
        //    get { return  MyFSM.CurrentState.StateName == StateName; }
        //}
	
	
    }
}