using System;

namespace Xtudio16.FSM
{
    public class FSMEvent
    {
	
        string name;
	
        public string Name {
            get {
                return name;
            }
        }
	
        public FSMEvent(string name)
        {
            this.name = name;
        }
    }
}